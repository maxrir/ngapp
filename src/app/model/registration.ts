
export class Registration {
    constructor(
      public id: number = 0,
      public nome: string = '',
      public idade: number = null,
      public sexo: string = 'M',
      public peso: number = null,
      public altura: number = null,
    ) { }
}