import { Injectable } from '@angular/core';
import {Registration} from './model/registration';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PersonServiceService {
  [x: string]: any;
  server: string = 'http://localhost:65488/api/';
  registrations: Registration[] = [];
  constructor(private http: HttpClient) { }
  
  onGetAll() {
    this.http.get(this.server + "list").toPromise().
      then((result: Array<any>) => {
        let array: Array<any> = result;
        this.registrations = [];
        for (let entry of array) {
          this.registrations.push(new Registration(entry.Id, entry.Nome, entry.Idade, entry.Sexo, entry.Peso, entry.Altura));
        }
      }).
      catch((err) => {
        console.log(err);
      });

  }

  onPostPerson(item: Registration) {
    item.altura = ((item.altura > 0) ? item.altura : null);
    this.http.post(this.server + "insert", item).toPromise().
      then((result) => {
        if (result == 'OK')
          this.onGetAll();
        else
          alert('Ocorreu uma Falha ao Adicionar o Usuário')
      }).
      catch((err) => {
        console.log(err);
      });
  }

  onPutPerson(item: Registration) {
    item.altura = ((item.altura > 0) ? item.altura : null);
    this.http.put(this.server + "update", item).toPromise().
      then((result) => {
        if (result == 'OK')
          this.onGetAll();
        else
          alert('Ocorreu uma Falha ao Atualizar o Usuário')
      }).
      catch((err) => {
        console.log(err);
      });
  }

  onDeletePerson(id: number) {
    this.http.delete(this.server + "delete/" + id).toPromise().
      then((result) => {
        if (result == 'OK')
          this.onGetAll();
        else
          alert('Ocorreu uma Falha ao Excluir o Usuário')
      }).
      catch((err) => {
        console.log(err);
      });
  }
}
