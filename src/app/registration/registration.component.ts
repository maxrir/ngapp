import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {Registration} from '../model/registration';
import {PersonServiceService} from '../person-service.service'

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
@Injectable()
export class RegistrationComponent implements OnInit {
  [x: string]: any;

  form: FormGroup;
  regModel: Registration;
  showNew: Boolean = false;
  submitType: string = 'Save';
  selectedRow: number;
  sexos: string[] = ['M', 'F'];
  
  
  constructor(private service: PersonServiceService) {
    this.service.onGetAll();
  }

  ngOnInit() {
  }

  onNew() {
    this.regModel = new Registration();
    this.submitType = 'Salvar';
    this.showNew = true;
  }

  onSave() {
    if (this.submitType === 'Salvar') {
      this.service.onPostPerson(this.regModel);
    } else {
      this.service.onPutPerson(this.regModel);
    }
    this.showNew = false;
  }

  onEdit(index: number) {
    this.selectedRow = index;
    this.regModel = new Registration();
    this.regModel = Object.assign({}, this.service.registrations[this.selectedRow]);
    this.submitType = 'Alterar';
    this.showNew = true;
  }

  onDelete(id: number) {
    debugger;
    this.service.onDeletePerson(id);
  }

  onCancel() {
    this.showNew = false;
  }

  onChangeSexo(sexo: string) {
    this.regModel.sexo = sexo;
  }
}
